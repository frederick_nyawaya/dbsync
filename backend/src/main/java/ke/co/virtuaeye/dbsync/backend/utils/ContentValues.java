/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.virtuaeye.dbsync.backend.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Frederick
 */
public class ContentValues {

    List<KeyValue> values;

    public ContentValues() {
        this.values = new ArrayList<>();
    }

    public synchronized void put(String key, String value) {

        Iterator<KeyValue> iterator = values.iterator();

        while (iterator.hasNext()) {
            KeyValue kv = iterator.next();
            if (kv.key.equals(key)) {
                iterator.remove();
               // values.remove(kv);
            }

        }

        values.add(new KeyValue(key, value));
    }
    
    public void remove(String key){
         Iterator<KeyValue> iterator = values.iterator();

        while (iterator.hasNext()) {
            KeyValue kv = iterator.next();
            if (kv.key.equals(key)) {
                iterator.remove();
               // values.remove(kv);
            }

        }
    }
    public void clear(){
        values.clear();
    }

    public List<KeyValue> getValues() {
        return values;
    }

    public void setValues(List<KeyValue> values) {
        this.values = values;
    }

    public class KeyValue {

        public String key, value;

        public KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }
}
