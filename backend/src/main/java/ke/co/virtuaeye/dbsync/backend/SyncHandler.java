/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.virtuaeye.dbsync.backend;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ke.co.virtuaeye.dbsync.backend.db.Database;
import ke.co.virtuaeye.dbsync.backend.utils.ContentValues;

/**
 * @author Frederick
 */
public class SyncHandler extends Database {

    private static SyncHandler me;

    public static final String TBL_PAYMENTS_OLD = "tblpayment";
    public static final String TBL_PAYMENTS_NEW = "payment";
    public static final String TBL_USERS_NEW = "rcuser";

    public static final String TBL_USERS_OLD = "tblusers";


    public static final String CN_ID = "id";
    public static final String CN_PAYMENT_MODE_ID = "paymentmode_id";
    public static final String CN_COST = "cost";
    public static final String CN_CENTER_ID = "center_id";

    public static final String C_RECEIVABLE_ID = "receivable_id";
    public static final String C_USER_ID = "user_id";

    public static final String CO_AMOUNT = "amount";
    public static final String CO_ID = "transaction_id";
    public static final String CO_PAYMENT_MODE_ID = "payment_mode_id";
    public static final String CO_WARD_ID = "ward_id";

    public static final String CO_USER_NAME = "username";
    public static final String CO_PASSWORD = "password";
    public static final String CO_WARD = "ward";

    public static final String CO_FIRST_NAME = "firstname";
    public static final String CO_SECOND_NAME = "secondname";


    private SyncHandler() throws SQLException {

    }

    public static SyncHandler getInstance() throws SQLException {
        if (me == null) {
            me = new SyncHandler();
        }

        return me;
    }

    public int[] syncPayments(String fromId) throws SQLException {
        //get records from new db -> push to old, only relevant row
        //opens connection witht the new db, returns a result object with the connect and result set
        Result newRecords = query(
                Config.URL + "/" + Config.DB_NEW,
                new String[]{TBL_PAYMENTS_NEW, TBL_USERS_NEW}, "where payment.user_id = rcuser.id AND payment.id >= ?", new String[]{fromId});

        ResultSet rs = newRecords.getResultSet();
        //create ContentValues for insert here
        List<ContentValues> values = new ArrayList<>();

        while (rs.next()) {
            ContentValues vals = new ContentValues();
            vals.put(CO_ID, rs.getString(CN_ID));
            vals.put(CO_PAYMENT_MODE_ID, rs.getString(CN_PAYMENT_MODE_ID));
            vals.put(C_RECEIVABLE_ID, rs.getString(C_RECEIVABLE_ID));
            vals.put(CO_AMOUNT, rs.getString(CN_COST));
            vals.put(C_USER_ID, rs.getString(C_USER_ID));
            vals.put(CO_WARD_ID, rs.getString(CN_CENTER_ID));


            values.add(vals);

        }

        rs.close();
        newRecords.getConnection().close();

        //now do the insert, might open many connections, so perform updates after each balh blah on the server
        int count = 0;
        int failed = 0;
        for (ContentValues cv : values) {
            try {
                Result res = insert(Config.URL + "/" + Config.DB_OLD, TBL_PAYMENTS_OLD, cv);
                res.getConnection().close();
                count += res.getCount();
            } catch (SQLException e) {
                e.printStackTrace();
                failed++;
            }
        }
        return new int[]{count, failed};

    }

    /**
     * Fetches fresh collector from old db, inserts in parallel db. the id MUST belong to a collector, its up to the caller to handle this
     *
     * @param id
     * @return
     * @throws SQLException
     */
    public int insertCollector(Long id) throws SQLException {
        Result resultObj = query(
                Config.URL + "/" + Config.DB_OLD,
                new String[]{TBL_USERS_OLD}, "where user_id = ?",
                new String[]{String.valueOf(id)});

        ResultSet rs = resultObj.getResultSet();
        int count = -1;
        if (rs.next()) {
            ContentValues values = new ContentValues();

            values.put(CN_ID, rs.getString(CO_USER_NAME));
            values.put("password", rs.getString(CO_PASSWORD));
            values.put("firstname", rs.getString(CO_FIRST_NAME));
            values.put("lastname", rs.getString(CO_SECOND_NAME));
            values.put("center_id", rs.getString(CO_WARD));

            rs.close();
            resultObj.getConnection().close();

            Result resObj = insert(Config.URL + "/" + Config.DB_NEW, TBL_USERS_NEW, values);
            count = resObj.getCount();
            resObj.getConnection().close();
        }


        return count;


    }

}
