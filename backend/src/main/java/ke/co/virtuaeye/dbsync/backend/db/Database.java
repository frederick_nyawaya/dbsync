/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ke.co.virtuaeye.dbsync.backend.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import ke.co.virtuaeye.dbsync.backend.Config;
import ke.co.virtuaeye.dbsync.backend.utils.ContentValues;

/**
 * Helper class to streamline db access - CRUD etc
 *
 * @author Frederick
 */
public abstract class Database {

    public Database() throws SQLException {
        DriverManager.registerDriver(new org.postgresql.Driver());

    }

    public Result rawQuery(String url, String queryString) throws SQLException {
        System.out.println(queryString);
        Connection conn = DriverManager.getConnection(url, Config.USER_NAME, Config.PASSWORD);
        PreparedStatement stmt = conn.prepareStatement(queryString);
        ResultSet rs = stmt.executeQuery();
        return new Result(conn, rs, -1);

    }

    /**
     * @param tables
     * @param where
     * @param whereArgs
     * @return
     * @throws SQLException
     */
    public Result query(String url, String[] tables, String where, String whereArgs[]) throws SQLException {
        Connection conn = DriverManager.getConnection(url, Config.USER_NAME, Config.PASSWORD);

        String tablesString = "";

        for (int i = 0; i < tables.length; i++) {
            tablesString += (tables[i] + (i < (tables.length - 1) ? "," : ""));
        }

        PreparedStatement stmt = conn.prepareStatement("select * from " + tablesString + " " + where);

        for (int i = 1; i <= whereArgs.length; i++) {

            if (isNumeric(whereArgs[i - 1])) {
                stmt.setLong(i, Long.parseLong(whereArgs[i - 1]));

            } else {
                stmt.setString(i, whereArgs[i - 1]);
            }
        }

        System.out.println(stmt.toString());

        ResultSet rs = stmt.executeQuery();
        return new Result(conn, rs, -1);
    }

    private boolean isNumeric(String number) {

        try {
            Long.parseLong(number);
            return true;

        } catch (NumberFormatException ex) {
            return false;
        }
    }

    /**
     * @param tableName
     * @param where     include "where"
     * @param whereArgs
     * @param sortOrder prefix with space
     * @return
     * @throws SQLException
     */
    public Result query(String url, String tableName, String where, String[] whereArgs, String sortOrder) throws SQLException {
        Connection conn = null;

        if (sortOrder == null) {
            sortOrder = "";
        }

        conn = DriverManager.getConnection(url, Config.USER_NAME, Config.PASSWORD);
        PreparedStatement stmt = conn.prepareStatement("select * from " + tableName + " " + where + sortOrder);

        if (where != null) {
            for (int i = 1; i <= whereArgs.length; i++) {
                stmt.setString(i, whereArgs[i - 1]);
            }
        }

        ResultSet rs = stmt.executeQuery();
        return new Result(conn, rs, -1);

        //conn.close();
    }

    public Result update(String url, String table, ContentValues values, String whereClause, String[] whereArgs) throws SQLException {
        String setSeg = " set ";
        List<ContentValues.KeyValue> kvList = values.getValues();

        Connection conn = null;
        boolean result;

        conn = DriverManager.getConnection(url, Config.USER_NAME, Config.PASSWORD);

        for (ContentValues.KeyValue kv : kvList) {
            String keyVal = kv.value.equals("sysdate") ? "sysdate" : "'" + kv.value + "'";

            setSeg += kv.key + "=" + keyVal + (kvList.indexOf(kv) < kvList.size() - 1 ? "," : " ");
        }
        PreparedStatement stmt = conn.prepareStatement("update " + table + setSeg + whereClause);

        System.out.println(setSeg);
        for (int i = 1; i <= whereArgs.length; i++) {
            stmt.setString(i, whereArgs[i - 1]);
        }

        return new Result(conn, null, stmt.executeUpdate());
    }

    public Result insert(String url, String table, ContentValues values) throws SQLException {
        List<ContentValues.KeyValue> valuesList = values.getValues();
        String columns = "(";
        String valuesString = "(";
        for (ContentValues.KeyValue keyValue : valuesList) {
            columns += keyValue.key;
            System.out.println("KEY -- " + keyValue.key + " VALUE -- " + keyValue.value);
            if (keyValue.value.equals("sysdate")) {
                valuesString += "sysdate ";
            } else {
                valuesString += "?";
            }
            if (valuesList.indexOf(keyValue) < valuesList.size() - 1) {
                columns += ",";
                valuesString += ",";
            } else {
                columns += ")";
                valuesString += ")";

            }

        }

        return insert(url, table, columns, valuesString, values);

    }

    private Result insert(String url, String table, String columns, String values, ContentValues cvs) throws SQLException {
        String statement = "INSERT INTO " + table + columns + " VALUES" + values;
        Connection conn = DriverManager.getConnection(url, Config.USER_NAME, Config.PASSWORD);
        PreparedStatement stmt = conn.prepareStatement(statement);
        List<ContentValues.KeyValue> valuesList = cvs.getValues();
        int i = 1;
        for (ContentValues.KeyValue kv : valuesList) {
            if (!kv.value.equals("sysdate")) {
                if (isNumeric(kv.value)) {
                    stmt.setLong(i++, Long.parseLong(kv.value));

                } else {
                    stmt.setString(i++, kv.value);
                }
            }

        }
        Logger.getLogger(Database.class
                .getName()).log(Level.SEVERE, statement, statement);
        int success = stmt.executeUpdate();
        conn.close();
        stmt.close();

        System.out.println("inserted sumtin");

        return new Result(conn, null, success);
    }

    public class Result {

        Connection connection;
        ResultSet resultSet;
        int count;

        public Result(Connection connection, ResultSet resultSet, int count) {
            this.connection = connection;
            this.resultSet = resultSet;
            this.count = count;
        }

        public void closeAll() throws SQLException {
            connection.close();
            resultSet.close();
        }

        public Connection getConnection() {
            return connection;
        }

        public void setConnection(Connection connection) {
            this.connection = connection;
        }

        public ResultSet getResultSet() {
            return resultSet;
        }

        public void setResultSet(ResultSet resultSet) {
            this.resultSet = resultSet;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

    }

}
